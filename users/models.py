from django.contrib.auth.models import AbstractUser
from django.db import models


def upload_profile_image_to(instance):
    return f'profile_images/{instance.pk}/profile_image.png'


def get_default_profile_image():
    return 'images/logo_1080_1080.png'


class CustomUser(AbstractUser):
    # override to make email unique
    email = models.EmailField(verbose_name='email', max_length=60, unique=True)
    profile_image = models.ImageField(
        max_length=255, upload_to=upload_profile_image_to, null=True, blank=True, default=get_default_profile_image)
    hide_email = models.BooleanField(default=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def get_profile_image_filename(self):
        image_str = str(self.profile_image)
        return image_str[image_str.index(f'profile_images/{self.pk}/'):]
