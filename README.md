# djangochannels-chat-server

A Django Channels project to learn and experiment.

---

[TOC]

## technology

- django
- channels
- django rest framework
- docker
- postgresql
- redis

## docker-compose.yml

```yaml
version: "3"

services:
    web:
        build: .
        # command: gunicorn core.wsgi -b 0.0.0.0:8000 # gunicorn対応
        command: python /app/manage.py runserver 0.0.0.0:8000
        environment: 
            APPLICATION_NAME: djangochannels-chat-server
            ENVIRONMENT: develop
            SECRET_KEY:
            DEBUG: 1
            CLOUDINARY_NAME:
            CLOUDINARY_API_KEY:
            CLOUDINARY_API_SECRET:
        volumes:
            - .:/app
        ports:
            - 8000:8000
        depends_on:
            - redis
            - db
    redis:
        image: redis:6.0.9-alpine
        ports:
            - "6379:6379"
    db:
        image: postgres:11
        volumes:
            - postgres_data:/var/lib/postgresql/data/
        environment:
            - "POSTGRES_HOST_AUTH_METHOD=trust"

volumes:
    postgres_data:
```
