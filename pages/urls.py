from django.urls import path

from .views import HomeScreenView

app_name = 'pages'

urlpatterns = [
    path('', HomeScreenView.as_view(), name='home'),
]
