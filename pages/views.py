from django.shortcuts import render
from django.views.generic import TemplateView


class HomeScreenView(TemplateView):
    template_name = 'pages/home.html'
